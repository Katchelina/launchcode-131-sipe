package studio1;

import cse131.ArgsProcessor;

public class Average {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		//write code to print the average of two integer inputs
		ArgsProcessor ap = new ArgsProcessor(args);
		double a = ap.nextInt("What is the first number?");
		double b = ap.nextInt("What is the second number?");
		
		double c = (a + b) / 2;
		
		System.out.println(c);
		
	}
}
