package exercises1;

import cse131.ArgsProcessor;

public class Change {

	public static void main(String[] args) {
		//
		// Below, prompt the user to enter a number of pennies
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		int pennies = ap.nextInt("How many pennies?");
		
		//
		// Then, compute and print out how many 
		//    dollars, quarters, dimes, nickels, and pennies
		// should be given in exchange for those pennies, so as to
		// minimize the number of coins (see the videos)
		//
		int dollars = pennies / 100;
		int leftoverPennies = pennies % 100;		
		int quarters = leftoverPennies / 25;	
		int dimes = quarters / 10;
		int nickels = dollars / 5;

		System.out.println("You have $"+dollars+" and "+leftoverPennies+" cents left over.");
		System.out.println("The pennies make " + quarters + " quarters, "+dimes +" dimes, and "+ nickels +" nickels left over.");
		
	}

}
