package exercises2;

import cse131.ArgsProcessor;

public class FeetInches {

	public static void main(String[] args) {
		//
		// Prompt the user for a number of inches
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		int inches = ap.nextInt("How many inches?");
		// Convert that into feet and inches
		//   BUT
		// Be sure to use the singular "foot" or "inch"
		//   where appropriate, as discussed in
		//   the introductory video
		//
		int feet = inches/12;
		int inch = inches%12;
		if (feet >= 1) {
			if (inch <= 1){
				System.out.println(feet + " Foot " + inch + " Inch.");
			} else {
				System.out.println(feet + " Foot " + inch + " Inches");
			}
		} else {
			if (inch <= 1){
				System.out.println(feet + " Feet " + inch + " Inch.");
			} else {
				System.out.println(feet + " Feet " + inch + " Inches");
			}
			
		}
		// For example, 61 inches would produce
		//    the output
		//   5 feet and 1 inch
		//

	}

}
