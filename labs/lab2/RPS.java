package lab2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {

		/*In this lab you will simulate two players: one plays randomly while 
		the other rotates faithfully from rock to paper to scissors.
		Your task is to simulate this game and to report the fraction 
		of games won by each of the two players.*/
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int rounds = ap.nextInt("How many rounds of RPS would you like to play?");
		System.out.println("You chose to play "+ rounds +" rounds of RPS.");
		double straightWins = 0;
		double draws = 0;
		double numRounds = 0;
		double randomWins = 0;
		for (int i = 1; i <= rounds; i++){	
			int randomHand = 0;
			int straightHand = 0;
			if (i%3 == 0){ //STRAIGHTHAND USES ROCK IF ROUND NUMBER IS DIVISIBLE BY 3
				straightHand = 1; //rock
				randomHand = 0;
				if (Math.random()<(1.0/3.0)){
					randomHand = 1; //rock
				} else if (Math.random()<(2.0/3.0)){
					randomHand = 2; //paper
				} else if (Math.random()<(3.0/3.0)){
					randomHand = 0; //scissors
				}	
				if (randomHand>straightHand){
					System.out.println("Round: " + i + "   The winner is RandomHand.");	
					randomWins++;
					numRounds++;
				} else if (randomHand == straightHand){
					System.out.println("Round: " + i + "   The game is a draw.");
					draws++;
					numRounds++;
				} else {
					System.out.println("Round: " + i + "   The winner is StraightHand.");
					straightWins++;
					numRounds++;
				}
			} else if (i%2 == 0){ //STRAIGHTHAND USES PAPER IF ROUND NUMBER IS DIVISIBLE BY 2 BUT NOT 3
				straightHand = 1; //paper
				randomHand = 0;
				if (Math.random()<(1.0/3.0)){
					randomHand = 0; //rock
				} else if (Math.random()<(2.0/3.0)){
					randomHand = 1; //paper
				} else if (Math.random()<(3.0/3.0)){
					randomHand = 2; //scissors
				}
				if (randomHand>straightHand){
					System.out.println("Round: " + i + "   The winner is RandomHand.");	
					randomWins++;
					numRounds++;
				} else if (randomHand == straightHand){
					System.out.println("Round: " + i + "   The game is a draw.");
					draws++;
					numRounds++;
				} else {
					System.out.println("Round: " + i + "   The winner is StraightHand.");
					straightWins++;
					numRounds++;
				}
			} else {			  //STRAIGHTHAND USES SCISSORS IF ROUND NUMBER IS NOT DIVISIBLE BY 2 OR 3
				straightHand = 1; //scissors
				randomHand = 0;
				if (Math.random()<(1.0/3.0)){
					randomHand = 2; //rock
				} else if (Math.random()<(2.0/3.0)){
					randomHand = 0; //paper
				} else if (Math.random()<(3.0/3.0)){
					randomHand = 1; //scissors
				}		
				if (randomHand>straightHand){
					System.out.println("Round: " + i + "   The winner is RandomHand.");	
					randomWins++;
					numRounds++;
				} else if (randomHand == straightHand){
					System.out.println("Round: " + i + "   The game is a draw.");
					draws++;
					numRounds++;
				} else {
					System.out.println("Round: " + i + "   The winner is StraightHand.");
					straightWins++;
					numRounds++;
				}
			}	
		}	double straightPercent = ((straightWins/numRounds)*100);
			double straightPercent2 = Math.round(straightPercent*100.0)/100.0;
			double randomPercent = (randomWins/numRounds)*100;
			double randomPercent2 = Math.round(randomPercent*100.0)/100.0;
			double drawPercent = (draws/numRounds)*100;
			double drawPercent2 = Math.round(drawPercent*100.0)/100.0;
			
			System.out.println("StraightHand won " + straightPercent2 + "% of the time.");
			System.out.println("RandomHand won " + randomPercent2 + "% of the time.");
			System.out.println("There were draws " + drawPercent2 + "% of the time.");
		
	} 
}
