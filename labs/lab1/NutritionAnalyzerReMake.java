package lab1;

import cse131.ArgsProcessor;

public class NutritionAnalyzerReMake {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	ArgsProcessor ap = new ArgsProcessor(args);
	String name = ap.nextString("What is the name of the food?");
	double carbs = ap.nextDouble("How many grams of carbs are in "+name+"?");
	double fat = ap.nextDouble("How many grams of fat are in it?");
	double protein = ap.nextDouble("How many grams of protein are in it?");
	double statedCals = ap.nextDouble("How many calories are in it?");
	
	double calsCarbs = carbs * 4.0;
	double calsPro = protein * 4.0;
	double calsFat = fat * 9.0;
	double totalCals = calsCarbs + calsPro + calsFat;
	double fiber = (totalCals - statedCals);
	fiber = Math.round(fiber*10)/10.0;
	double fiberGrams = fiber/4;
	double percentCarbs = calsCarbs / statedCals;
	double percentFat = calsFat / statedCals;
	double percentProtein = calsPro / statedCals;
	percentCarbs = (percentCarbs*100.0);
	percentCarbs = Math.round(percentCarbs*10.0)/10.0;
	percentFat = (percentFat*100);
	percentFat = Math.round(percentFat*10.0)/10.0;
	percentProtein = (percentProtein*100);
	percentProtein = Math.round(percentProtein*10.0)/10.0;
	boolean lowCarb = (calsCarbs < (totalCals * 0.25));
	boolean lowFat = (calsFat < (totalCals * 0.15));
	boolean flip = (Math.random() < 0.5);
	
System.out.println("This food is said to have "+statedCals+" (Available) Calories.");
System.out.println("With "+fiber+" unavailable Calories, this food has "+fiberGrams+" grams of fiber.");
System.out.println(" ");
System.out.println("Approximately;");
System.out.println("  " + percentCarbs + "% of your food is carbohydrates.");
System.out.println("  " + percentFat + "% of your food is fat.");
System.out.println("  " + percentProtein + "% of your food is protein.");
System.out.println("This food is acceptable for a low-carb diet? " + lowCarb);
System.out.println("This food is acceptable for a low-fat diet? " + lowFat);
System.out.println("By coin flip, you should eat this food? " + flip);
}}
